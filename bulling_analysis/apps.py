from django.apps import AppConfig


class BullingAnalysisConfig(AppConfig):
    name = 'bulling_analysis'
